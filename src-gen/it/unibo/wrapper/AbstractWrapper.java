/* Generated by AN DISI Unibo */ 
package it.unibo.wrapper;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import it.unibo.qactors.QActorContext;
import it.unibo.qactors.ActorTerminationMessage;
import it.unibo.qactors.QActorMessage;
import it.unibo.qactors.QActorUtils;
import it.unibo.contactEvent.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.action.ActionReceiveTimed;
import it.unibo.qactors.action.AsynchActionResult;
import it.unibo.qactors.action.IActorAction;
import it.unibo.qactors.action.IActorAction.ActionExecMode;
import it.unibo.qactors.action.IMsgQueue;
import it.unibo.qactors.akka.QActor;


//REGENERATE AKKA: QActor instead QActorPlanned
public abstract class AbstractWrapper extends QActor { 
	protected AsynchActionResult aar = null;
	protected boolean actionResult = true;
	protected alice.tuprolog.SolveInfo sol;
	//protected IMsgQueue mysupport ;  //defined in QActor
	protected String planFilePath    = null;
	protected String terminationEvId = "default";
	protected String parg="";
	protected boolean bres=false;
	protected IActorAction  action;
	
			protected static IOutputEnvView setTheEnv(IOutputEnvView outEnvView ){
				return outEnvView;
			}
	
	
		public AbstractWrapper(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
			super(actorId, myCtx,  
			"./srcMore/it/unibo/wrapper/WorldTheory.pl",
			setTheEnv( outEnvView )  , "init");		
			this.planFilePath = "./srcMore/it/unibo/wrapper/plans.txt";
			//Plan interpretation is done in Prolog
			//if(planFilePath != null) planUtils.buildPlanTable(planFilePath);
	 	}
		@Override
		protected void doJob() throws Exception {
			String name  = getName().replace("_ctrl", "");
			mysupport = (IMsgQueue) QActorUtils.getQActor( name );
	 		initSensorSystem();
			boolean res = init();
			//println(getName() + " doJob " + res );
		} 
		/* 
		* ------------------------------------------------------------
		* PLANS
		* ------------------------------------------------------------
		*/
	    public boolean init() throws Exception{	//public to allow reflection
	    try{
	    	curPlanInExec =  "init";
	    	boolean returnValue = suspendWork;
	    while(true){
	    nPlanIter++;
	    		temporaryStr = " \"Sonar wrapper starts\" ";
	    		println( temporaryStr );  
	    		//senseEvent
	    		timeoutval = 600000;
	    		aar = planUtils.senseEvents( timeoutval,"numOfSonar","continue",
	    		"" , "",ActionExecMode.synch );
	    		if( ! aar.getGoon() || aar.getTimeRemained() <= 0 ){
	    			//println("			WARNING: sense timeout");
	    			addRule("tout(senseevent,"+getName()+")");
	    			//break;
	    		}
	    		//onEvent
	    		if( currentEvent.getEventId().equals("numOfSonar") ){
	    		 		String parg = "actorOp(configureTheSystem(N))";
	    		 		parg =  updateVars( Term.createTerm("numOfSonar(N)"), Term.createTerm("numOfSonar(N)"), 
	    		 			    		  					Term.createTerm(currentEvent.getMsg()), parg);
	    		 		if( parg != null ){
	    		 				aar = solveGoalReactive(parg,3600000,"","");
	    		 				//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    		 				if( aar.getInterrupted() ){
	    		 					curPlanInExec   = "init";
	    		 					if( ! aar.getGoon() ) break;
	    		 				} 			
	    		 		}
	    		 }
	    		if( (guardVars = QActorUtils.evalTheGuard(this, " !?actorOpDone(OP,R)" )) != null ){
	    		temporaryStr = QActorUtils.unifyMsgContent(pengine, "sonarSubsystemReady","sonarSubsystemReady", guardVars ).toString();
	    		emit( "sonarSubsystemReady", temporaryStr );
	    		}
	    		if( ! planUtils.switchToPlan("doWork").getGoon() ) break;
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	       //println( getName() + " plan=init WARNING:" + e.getMessage() );
	       QActorContext.terminateQActorSystem(this); 
	       return false;  
	    }
	    }
	    public boolean doWork() throws Exception{	//public to allow reflection
	    try{
	    	curPlanInExec =  "doWork";
	    	boolean returnValue = suspendWork;
	    while(true){
	    nPlanIter++;
	    		if( (guardVars = QActorUtils.evalTheGuard(this, " ??msg( \"restart\" , \"event\" ,S,none,M,N)" )) != null ){
	    		if( ! planUtils.switchToPlan("restartWrapper").getGoon() ) break;
	    		}
	    		//senseEvent
	    		timeoutval = 600000;
	    		aar = planUtils.senseEvents( timeoutval,"sonar,restart","continue,restartWrapper",
	    		"" , "",ActionExecMode.synch );
	    		if( ! aar.getGoon() || aar.getTimeRemained() <= 0 ){
	    			//println("			WARNING: sense timeout");
	    			addRule("tout(senseevent,"+getName()+")");
	    			//break;
	    		}
	    		//onEvent
	    		if( currentEvent.getEventId().equals("sonar") ){
	    		 		String parg = "actorOp(evaluateExp(D,POS))";
	    		 		parg =  updateVars( Term.createTerm("p(Distance,Pos)"), Term.createTerm("p(D,POS)"), 
	    		 			    		  					Term.createTerm(currentEvent.getMsg()), parg);
	    		 		if( parg != null ){
	    		 				aar = solveGoalReactive(parg,3600000,"","");
	    		 				//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    		 				if( aar.getInterrupted() ){
	    		 					curPlanInExec   = "doWork";
	    		 					if( ! aar.getGoon() ) break;
	    		 				} 			
	    		 		}
	    		 }
	    		if( (guardVars = QActorUtils.evalTheGuard(this, " !?actorOpDone(OP, \"alarm\" )" )) != null ){
	    		//playsound
	    		terminationEvId = "endplay";
	    			 	aar = playSound("./audio/bido_bido.wav",ActionExecMode.asynch, terminationEvId, 3000,"" , "" ); 
	    				//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    				if( aar.getInterrupted() ){
	    					curPlanInExec   = "doWork";
	    					if( ! aar.getGoon() ) break;
	    				} 			
	    		}
	    		if( (guardVars = QActorUtils.evalTheGuard(this, " ??actorOpDone(OP, \"alarm\" )" )) != null ){
	    		temporaryStr = QActorUtils.unifyMsgContent(pengine, "alarm","alarm", guardVars ).toString();
	    		emit( "alarm", temporaryStr );
	    		}
	    		if( (guardVars = QActorUtils.evalTheGuard(this, " ??actorOpDone(OP, \"less\" )" )) != null ){
	    		temporaryStr = QActorUtils.unifyMsgContent(pengine, "turn","turn", guardVars ).toString();
	    		emit( "turn", temporaryStr );
	    		}
	    		if( planUtils.repeatPlan(0).getGoon() ) continue;
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	       //println( getName() + " plan=doWork WARNING:" + e.getMessage() );
	       QActorContext.terminateQActorSystem(this); 
	       return false;  
	    }
	    }
	    public boolean restartWrapper() throws Exception{	//public to allow reflection
	    try{
	    	curPlanInExec =  "restartWrapper";
	    	boolean returnValue = suspendWork;
	    while(true){
	    nPlanIter++;
	    		parg = "actorOp(restart)";
	    		aar = solveGoalReactive(parg,3600000,"","");
	    		//println(getName() + " plan " + curPlanInExec  +  " interrupted=" + aar.getInterrupted() + " action goon="+aar.getGoon());
	    		if( aar.getInterrupted() ){
	    			curPlanInExec   = "restartWrapper";
	    			if( ! aar.getGoon() ) break;
	    		} 			
	    		if( ! planUtils.switchToPlan("doWork").getGoon() ) break;
	    break;
	    }//while
	    return returnValue;
	    }catch(Exception e){
	       //println( getName() + " plan=restartWrapper WARNING:" + e.getMessage() );
	       QActorContext.terminateQActorSystem(this); 
	       return false;  
	    }
	    }
	    protected void initSensorSystem(){
	    	//doing nothing in a QActor
	    }
	    
	 
		/* 
		* ------------------------------------------------------------
		* APPLICATION ACTIONS
		* ------------------------------------------------------------
		*/
		/* 
		* ------------------------------------------------------------
		* QUEUE  
		* ------------------------------------------------------------
		*/
		    protected void getMsgFromInputQueue(){
	//	    	println( " %%%% getMsgFromInputQueue" ); 
		    	QActorMessage msg = mysupport.getMsgFromQueue(); //blocking
	//	    	println( " %%%% getMsgFromInputQueue continues with " + msg );
		    	this.currentMessage = msg;
		    }
	  }
	
