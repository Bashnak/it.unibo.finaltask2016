/* Generated by AN DISI Unibo */ 
/*
This code is generated only ONCE
*/
package it.unibo.camera;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;

public class Camera extends AbstractCamera { 

	
	public Camera(String actorId, QActorContext myCtx, IOutputEnvView outEnvView )  throws Exception{
		super(actorId, myCtx, outEnvView);
	}	
	
	
	public byte[] extractBytes (String ImageName) throws IOException {
		 // open image
		 File imgPath = new File(ImageName);
		 BufferedImage bufferedImage = ImageIO.read(imgPath);

		 // get DataBufferBytes from Raster
		 WritableRaster raster = bufferedImage .getRaster();
		 DataBufferByte data   = (DataBufferByte) raster.getDataBuffer();

		 return ( data.getData() );
		}
	
	public String takePicture() throws IOException{

		try {
  	  		//Execute a C program that generates the sonar values
			Process p = Runtime.getRuntime().exec("sudo ./RaspCamera");
			p.waitFor();
			BufferedReader readerC = new BufferedReader(new java.io.InputStreamReader(p.getInputStream()));
		} catch (Exception e) {
 			e.printStackTrace();
		}	
		
		return "photo";
	}
}
