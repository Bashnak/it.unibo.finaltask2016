/* Generated by AN DISI Unibo */ 
package it.unibo.ctxRadarTfCe16;
import it.unibo.ctxRadarTfCe.AbstractEvh1;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.QActorContext;

public class Evh1 extends AbstractEvh1 { 
	public Evh1(String name, QActorContext myCtx, IOutputEnvView outEnvView, String[] eventIds ) throws Exception {
		super(name, myCtx, outEnvView,eventIds);
  	}
}
