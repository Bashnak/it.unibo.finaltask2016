package it.unibo.wrapper;

public class Sonar {
	
	private int dist;
	private int pos;
	
	public Sonar (int dist, int pos, int n) {
		this.dist = dist;
		this.pos = pos;
	}
	
	public Sonar() {
		// TODO Auto-generated constructor stub
	}

	public int getPos(){
		return pos;
	}
	
	public int getDist(){
		return dist;
	}
	
	public void setDist(int dist){
		this.dist = dist;
	}
	
	public void setPos(int pos){
		this.pos = pos;
	}
}
