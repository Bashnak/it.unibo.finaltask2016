package it.unibo.led;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import it.unibo.bls.highLevel.interfaces.IDevLed;
import it.unibo.bls.lowLevel.interfaces.ILed;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.system.SituatedActiveObject;

/*
 * NEW CONCEPT: an active object that performs an action in its own thread
 */
public class BlinkAsynch extends SituatedActiveObject  {
private ILed led;
private boolean goon = true;

	public BlinkAsynch(String name,IOutputEnvView outEnvView,ILed concreteLed) {
		super(outEnvView,name);
		this.led = concreteLed;
	}
	public void activate(){
		int numberOfCores = Runtime.getRuntime().availableProcessors();
		ScheduledExecutorService executorNumberOfCores = Executors.newScheduledThreadPool(numberOfCores);
		super.activate(executorNumberOfCores);
	}
	public void suspendAction(){
		goon = false;
	}
 
	@Override
	protected void startWork() throws Exception {
		goon = true;
	}
	@Override
	protected void doJob() throws Exception {
		while(goon){
			led.turnOn();
			//System.out.println("LED ACCESSO");
			delay(200);
			if( ! goon ) break;
			led.turnOff();
			//System.out.println("LED SPENTO");
			delay(200); 
		}
	}
	@Override
	protected void endWork() throws Exception {
		led.turnOff();
	}
	protected static void delay(int n) {
		try {
			Thread.sleep(n);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
