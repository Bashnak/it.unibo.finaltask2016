%====================================================================================
% Context ctxSensorEmitter1  SYSTEM-configuration: file it.unibo.ctxSensorEmitter1.sonarSensorEmitter.pl 
%====================================================================================
context(ctxradartfce16, "localhost",  "TCP", "8033" ).  		 
context(ctxsensoremitter1, "localhost",  "TCP", "8134" ).  		 
%%% -------------------------------------------
qactor( sensorsonar1 , ctxsensoremitter1, "it.unibo.sensorsonar1.MsgHandle_Sensorsonar1"   ). %%store msgs 
qactor( sensorsonar1_ctrl , ctxsensoremitter1, "it.unibo.sensorsonar1.Sensorsonar1"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

