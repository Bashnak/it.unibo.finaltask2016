%====================================================================================
% Context ctxConsole  SYSTEM-configuration: file it.unibo.ctxConsole.final.pl 
%====================================================================================
context(ctxrobot, "192.168.1.69",  "TCP", "8133" ).  		 
context(ctxconsole, "192.168.1.66",  "TCP", "8062" ).  		 
context(ctxradartfce, "192.168.1.66",  "TCP", "8033" ).  		 
%%% -------------------------------------------
qactor( console , ctxconsole, "it.unibo.console.MsgHandle_Console"   ). %%store msgs 
qactor( console_ctrl , ctxconsole, "it.unibo.console.Console"   ). %%control-driven 
qactor( radargui , ctxconsole, "it.unibo.radargui.MsgHandle_Radargui"   ). %%store msgs 
qactor( radargui_ctrl , ctxconsole, "it.unibo.radargui.Radargui"   ). %%control-driven 
qactor( mqttreader , ctxconsole, "it.unibo.mqttreader.MsgHandle_Mqttreader"   ). %%store msgs 
qactor( mqttreader_ctrl , ctxconsole, "it.unibo.mqttreader.Mqttreader"   ). %%control-driven 
qactor( mqttwriter , ctxrobot, "it.unibo.mqttwriter.MsgHandle_Mqttwriter"   ). %%store msgs 
qactor( mqttwriter_ctrl , ctxrobot, "it.unibo.mqttwriter.Mqttwriter"   ). %%control-driven 
qactor( camera , ctxrobot, "it.unibo.camera.MsgHandle_Camera"   ). %%store msgs 
qactor( camera_ctrl , ctxrobot, "it.unibo.camera.Camera"   ). %%control-driven 
qactor( led , ctxrobot, "it.unibo.led.MsgHandle_Led"   ). %%store msgs 
qactor( led_ctrl , ctxrobot, "it.unibo.led.Led"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------
qactor( robot , ctxrobot, "it.unibo.robot.MsgHandle_Robot" ). 
qactor( robot_ctrl , ctxrobot, "it.unibo.robot.Robot" ). 

