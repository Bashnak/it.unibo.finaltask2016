%====================================================================================
% Context ctxRadarTfCe16  SYSTEM-configuration: file it.unibo.ctxRadarTfCe16.wrapperSystem.pl 
%====================================================================================
context(ctxradartfce16, "192.168.1.66",  "TCP", "8033" ).  		 
%%% -------------------------------------------
qactor( wrapper , ctxradartfce16, "it.unibo.wrapper.MsgHandle_Wrapper"   ). %%store msgs 
qactor( wrapper_ctrl , ctxradartfce16, "it.unibo.wrapper.Wrapper"   ). %%control-driven 
qactor( repeater , ctxradartfce16, "it.unibo.repeater.MsgHandle_Repeater"   ). %%store msgs 
qactor( repeater_ctrl , ctxradartfce16, "it.unibo.repeater.Repeater"   ). %%control-driven 
%%% -------------------------------------------
%%% -------------------------------------------

